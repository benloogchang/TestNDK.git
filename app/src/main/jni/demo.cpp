//
// Created by zbl on 2021/8/30.
//
#include <jni.h>
#include <string>

/*
    extern关键字，C语言extern关键字用法详解 http://c.biancheng.net/view/404.html
    如果全局变量不在文件的开头定义，有效的作用范围将只限于其定义处到文件结束。如果在定义点之前的函数想引用该全局变量，则应该在引用之前用关键字 extern 对该变量作“外部变量声明”，表示该变量是一个已经定义的外部变量。有了此声明，就可以从“声明”处起，合法地使用该外部变量。
 */

extern "C" JNIEXPORT jstring JNICALL
Java_com_test_testndk_MainActivity_stringFromJNI(JNIEnv* env,jobject /* this */) {
    std::string hello = "Hello from C++";
    return env->NewStringUTF(hello.c_str());
}